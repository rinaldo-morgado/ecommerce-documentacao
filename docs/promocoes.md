# Conheçendo o CMS - Promoções

## Cupom de desconto

O ecommerce disponibiliza a opção de realizar promoções a seus clientes através do __Cupom de Desconto__.
O cupom pode ser único para um cliente, como pode ser para vários clientes, ele pode utilizar este cupom apenas uma vez, ou quantas vezes ele desejar, o cupom pode ser dado em falor fixo, como também em porcentagem.

Para cadastrar um novo cupom, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__.  
Preencha as informações conforme cupom desejado:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Código do Cupom
Este será o cupom expressamente dito que seu cliente irá digitar no momento da compra. Utilize nomenclaturas fáceis de usar, por exemplo, dias das mães, __MAESNAXYZ__, ou __50OFF__. Este campo permite caracteres alfanuméricos, underline(_) e hifen (-).

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tipo de desconto
Esta seleção define se o valor do cupom será dado em um preço fixo, exemplo R$50 de desconto, ou se será por porcentagem, como 10% no valor dos produtos[^1].

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor do desconto
Este será o valor de desconto que será aplicado aos produtos no carrinho.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Data início
Outra opção do ecommerce, é criar promoções agendadas, sendo assim, é possível incluir uma data inicial para a validade do cupom. Caso seu cliente tente utilizar este cupom antes desta data, ele não terá sucesso e receberá uma mensagem informando que esta promoção ainda não está ativa. 

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Data final
Assim como uma data inicial, é necessário uma data final da promoção. Caso o usuário tente informar este cupom, reecberpa uma mensagem de que esta promoção já se encerrou.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Para compras acima de
E possível aplicar descontos somente acima de algum valor definido neste campo. Então se sua promoção é de R$50 acima de R$XXX, cadastre neste campo o valor. Caso o usuário tente usar com valor inferior a este informado não conseguirá continuar. Caso não tenha preço mínimo, apenas informe R$0,00 no campo.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Restrição de cupom
Este campo permite que apenas um ou alguns clientes utilizem o mesmo. Cadastrando o e-mail do cliente, apenas ele poderá utilizar o cupom. É possível cadastrar mais de um e-mail neste campo, basta separar com ponto e vírgula `;`.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quantas vezes o mesmo cliente poderá utilizar este cupom de desconto?
Define se o cliente poderá utilizar apenas uma vez o cupom, ou usar até quando não for mais valido.

---

## Frete Grátis por Região

O ecommerce permite disponibilizar frete grátis para uma faixa de CEP específica. Semelhante ao cadastro de faixa de CEP na transportadora, também o cadatro de faixa para frete grátis. Para criar, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__.  
Preencha as informações seguintes:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome da região
Define o nome da região que receberá frete grátis.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CEP inicial
Define o CEP inicial que receberá o frete grátis.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CEP final
Define o CEP final que receberá o frete grátis.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo do pedido
Permite informar o valor mínimo do pedido para receber o frete grátis.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Prazo para entrega (dias)
Perminte informar a quantiadade de dias para a entrega do pedido ao cliente.


[^1]: O valor do cupom, seja fixo ou em porcentagem, será dado sempre nos produtos. Apenas nos produtos, ou seja, frete não recebe desconto do cupom.