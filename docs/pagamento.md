# Conheçendo o CMS - Formas de Pagamento

## Boleto

Umas das formas de pagamento no ecommerce é o Boleto bancário. Devido as novas regras da FEBRABAN, todos os boletos devem ser registrados. Neste módulo é possível disponibilizar ou não o boleto como forma de pagamento para seus clientes. São possíveis outras configurações, tais como;

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>Aplicação de desconto no boleto  
Define se o valor do desconto do boleto é aplicável apenas para os itens no carrinho ou também para o frete.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Número de dias para vencimento após a compra
Define o número de dias para o vencimento do boleto a partir da data de finalização da compra.

!!! error "" 
    Este número deve ser menor ou igual ao número de dias configurados no gateway. Consulte nossa equipe para questionar.

---
## Cartão de crédito

Outra forma de pagamento no ecommerce é o cartão de crédito. Onde as transações são realizadas com o auxílio de um gateway de pagamentos, este serve como um mediador entre o ecommerce e sua operadora de creédito (Cielo/Redecard).

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar forma de pagamento por cartão de crédito via SuperPay 
Através desta configuração, pode se escolher habilitar ou não a venda por cartão de crédito através do SuperPay (gateway de pagamento supracitado). Ao habilitar, todas as bandeiras cadastradas na operadora estarão disponível para o usuário.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Número máximo de parcelas
Define o número máximo de parcelar possíveis numa compra. Este valor depende de seu contrato com a operadora de crédito. Devido a isso, deve ser menor ou igual a que foi contratada com a operadora.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo da parcela
Define o valor minimo por parcela aceitável pela loja.

---


## Faturamento Direto

Nesta página será possível configurar as regras de pagamento para o faturamento direto. Esta forma de pagamento é como uma maneira de negociar com o cliente para que este pague diretamente ao lojista em prazo diferente ou em alguma forma de pagamento não disponível na plataforma, como cheque, parcelado em 45/90 dias, depósito bancário ou pagamento na retirada. Para criar uma nova regra, basta clicar no botão  __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__ e preencha os seguintes campos:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome:
Define o nome da regra de faturamento direto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Definir como opção padrão
Define se esse faturamento é padrão apra todos os clientes. Caso seja marcado como __não__, deverá ser vinculado ao cliente na página de edição do mesmo ([veja aqui](clientes/#editando_informacoes_de_um_cliente)).

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Número de parcelas
Define a quantidade de parcelas para ser realizado o pagamento.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo do pedido
Define o valor mínimo do pedido para se enquadrar nesta regra de faturamento direto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Observação interna
Destina-se a colocar alguma observação para esta regra.

---

## Itaú Shopline
Nesta página poderá ser definido se será utilizado o Itaú Shopline como forma de pagamento. Vale ressaltar que o Shopline abre um popup onde é gerado o boleto. Dependendo do contrato com o banco, é possível disponibilizar outras formas de pagamento como transferência (apenas para clientes Itaú).

---

## PagSeguro - Transparente
Nesta página poderá ser escolhido quais formas de pagamento pelo Pagseguro serão utilizadas. Por padrão, todas as transações entre ecommerce e Pagseguro são transparentes, o que significa que o usuário não sai do seu ecommerce para realizar o pagamento. Sendo assim, você poderá configurar três formas de pagamento disponíves transparentes, sendo estas:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Cartão de crédito no checkout transparente
Define se será disponibilizada a forma de pagamento por cartão de crédito através do checkout transparente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Boleto bancário no checkout transparente
Define se será disponibilizada a forma de pagamento por boleto bancário através do checkout transparente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Débito online no checkout transparente
Define se será disponibilizada a forma de pagamento por débito online através do checkout transparente.

Obs.:Não é necessário habilitar todas, apenas a que será utilizada.

---

## PayPal
Nesta página, poderá definir se o pagamento através do Paypal estará disponível ou não para os clientes.


