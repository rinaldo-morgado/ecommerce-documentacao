# Conheçendo o CMS - Marketing

## Mídias Sociais

A página de mídias sociais permite que sejam cadastradas todas as redes socias de seu negócio. Basta escolher a rede social, clicar no link e incluir o link de seu negócio. É possível também escolher ordem em que essas mídias aparecerão no rodapé de seu ecommerce.

Caso deseje incluir alguma outra rede social que não esteja listada nesta página, contate o suporte.

---

## Newsletter

O ecommerce conta com uma ferramenta de captura de e-mails para newsletter. Desta forma, suas campanhas do ecommerce podem ter mais sucesso. Para coletar a lista de cadastrados na newsletter, basta clicar em __<i class="fa fa-cloud-download" aria-hidden="true"></i>Exportar__ para ter um arquivo em __.csv__ com todos os cadastrados.

---

## Popup

No ecommerce existe a possibilidade de incluir um popup ao abrir o site ou ao sair.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Habilitar popup quando?
Nesta configuração, poderá definir quando o popup será aberto, se ao abrir o site, ao fechar ou nunca.

---

O ecommerce trabalha com dois tipos possíveis de popup, um nativo e um próprio.  

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tipo de popup que será aberto
Define qual tipo de popup que será aberto, nativo ou próprio.

---

Para configurar o __popup nativo__, configure os seguintes campos:

__Exemplo de popup nativo__  
![screenshot-popup-nativo](assets/img/prints/marketing/layout-popup-nativo.png "Popup nativo")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Título da Janela
Define o título do popup. Equivalente ao _"__Aproveite nossas promoções e novidades exclusivas!__"_ no print acima.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Conteúdo do Popup
Define o conteúdo do popup. Equivalente ao _"__Deixe seu e-mail e seja o primeiro a receber nossas novidades e promoções.__"_ no print acima.

---
Para configurar o __popup próprio__, configure os seguinte campo:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Conteúdo do popup 
Define o conteúdo do popup próprio, sendo possível incluir texto e HTML.

---

##SEO (Search Engine Optimization)

SEO é o conjunto de estratégias com o objetivo de potencializar e melhorar o posicionamento de um site nas páginas de resultados naturais (orgânicos) nos sites de busca. A plataforma do ecommerce é preparada para que você possa gerenciar todo o SEO, desde a página inicial até um produto cadastrado.

Para alterar o cadastro de SEO de alguma página do ecommerce, basta selecionar a página que deseja alterar e preencher as seguintes informações:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Title
A tag _title_ é uma das que mais influênciam no resultado das buscas. 
Deve ser o título da página.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> _Keywords_  
_Keywords_ propriamente ditas, são as palavras-chaves da página em questão.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Description  
Este campo se destina para a descrição da página e do conteúdo.

!!! type ""
    Mas e o SEO dos produtos? Ao salvar um produto, existe a opção de atualizar o SEO, esta opção vem marcada como SIM de padrão.
    
