# Conheçendo o CMS

## Início / Dashboard

A *Dashboard* ou *Início* é a primeira página do CMS a ser exibida.  
Nela consta informações de rápido acesso que poderão garantir o bom funcionamento de sua loja virtual. Você irá encontrar atalhos que lhe ajudarão a acompanhar suas vendas.

Segue o descritivo de cada um deles.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Pedidos

O atalho de novos pedidos, mostra a quantidade de novos pedidos feitos durante o dia.
Ao clicar nele você será redirecionado para a listagem de pedidos.

![screenshot_pedidos](assets/img/prints/dashboard/pedidos.png "Contador de novos pedidos diários") ![screenshot_pedidos2](assets/img/prints/dashboard/pedidos_2.png "Contador de novos pedidos diários")

---


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Vendas

O atalho de vendas mostrará o valor total de vendas do dia.
Ao clicar você será redirecionado para o relatório de volume de vendas.

![screenshot_vendas_volume](assets/img/prints/dashboard/vendas.png "Contador de valor de venda diário") 

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Novos Clientes

O atalho de novos cliente indica a quantidade de novos cadastros realizados no e-commerce.
Facilitando assim a liberação (quando necessário) do cliente ou configurando uma tabela de preço específico.

![screenshot_novos_clientes](assets/img/prints/dashboard/novos_clientes.png "Contador de valor de venda diário") 

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Pedidos com pagamento aprovado nos últimos 7 dias

Neste gráfico é mostrado os últimos 7 dias de pedidos feitos com o pagamento aprovado.
Ou seja, apenas pedidos que tiveram seu pagamento confirmado serão mostrados neste gráfico. Abaixo do título se encontra o link de acesso ao relatório completo com mais opções de filtro.

![screenshot-pagamento-aprovado-7dias](assets/img/prints/dashboard/pedido-pagamento-aprovado.png "Gráfico de pedidos com pagamento aprovado nos últimos 7 dias.")

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Status por Pedido

Relatório que mostra a contagem da quantidade de pedidos por status. Em cada status da compra, mostra a quantidade de pedidos nela.

![screenshot-pedidos-por-status](assets/img/prints/dashboard/status-pedido.png "Relatório de pedidos por status")

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Últimos Pedidos (24h)

Listagem de todos os pedidos realizados (independente de pagamento aprovado ou não) nas últimas 24 horas.

![screenshot-pedidos-24h](assets/img/prints/dashboard/pedidos-24h.png "Listagem de pedidos das últimas 24 horas.")

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Novos Clientes (24h)

Listagem de todos os novos clientes cadastrados nas últimas 24 horas.

![screenshot-novos-clientes-24h](assets/img/prints/dashboard/novos-clientes-24h.png "Listagem de novos cadastros nas últimas 24 horas.")

---

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> XML Google Shopping / Buscapé Company

A plataforma do q.commerce está preparada para integrar via xml com o <a href="http://www.google.com.br/ads/new/" target="_blank"> **Google Shopping**</a> e também com o <a href="http://www.buscapecompany.com/portal/buscape-company/home/" target="_blank"> **Buscapé Company**</a>.  
Basta clicar no botão **Copiar** para coletar o link do arquivo, após isso insira o mesmo na plataforma do Google / Buscapé. 

![screenshot-xml-gshopping](assets/img/prints/dashboard/xml-gshopping.png "Link do arquivo .xml para o Google Shopping")
![screenshot-xml-buscape-company](assets/img/prints/dashboard/xml-buscape.png "Link do arquivo .xml para o Buscapé Company")