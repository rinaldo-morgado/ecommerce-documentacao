# Conheçendo o CMS - Clientes

## Clientes

A tela de clientes, contém uma listagem de todos os clientes cadastrados na plataforma.  
Nesta página, você encontrará uma listagem com as seguintes colunas:

* __Status__
    - Aprovado SIM/NÃO
* __Nome completo - CPF__
    - Nome completo do cliente
    - CPF
    - CNPJ (quando tiver)
* __Contato__
    - E-mail
    - Telefone
* __Data de Cadastro__
    - Data em que o cliente foi cadastrado
* __Última Compra__
    - Data da última compra realizada
    - Quantidade de pedidos feitos

---

### Editando informações de um cliente

No botão __AÇÕES__, opção __EDITAR__, poderá ter acesso ao cadastro completo do cliente. Nesta página, poderá aprovar, reprovar ou bloquear o cliente, alterar as informações do cliente, verificar endereços de entrega cadastrados, selecionar a tabela de preços e também as opções de faturamento direto como forma de pagamento, quando habilitado.

---

## Tabelas de preço

O Qcommerce tem a possibilidade de trabalhar com diversas tabelas de preço. Se a sua empresa trabalha com valores diferenciados para pessoas físicas e jurídicas, ou atacado e varejo, a plataforma estará apta a atender a sua demanda. Vale lembrar que o cadastro de tabelas de preço não tem limite.

Para criar uma nova tabela, clique no botão  **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo** no canto superior direito da tela.  
Nesta página, você encontrará os campos necessários para a criação de uma nova tabela de preços.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome
Este será o nome da tabela no sistema. Na opção de vincular uma tabela a um cliente, este será o nome mostrado.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tipo de tabela  
Define que tipo de índice de variação nos valores dos produtos esta tabela terá.  

Deverá escolher uma opção:  

* Desejo aplicar um __desconto__ nos valores dos produtos. 
* Desejo aplicar um __acréscimo__ nos valores dos produtos.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Variação padrão  
Define o valor percentual de variação do valor do produto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Gerenciamento de novos produtos  
Pensando na comodidade do administrador, o Qcommerce disponibiliza a opção de atualizar automaticamente os novos produtos da plataforma.  

Para tal, basta selecionar a opção __"Sim, desejo que o sistema gerencie isso"__. Feito isso, ao criar um novo produto, de maneira automática, o Qcommerce irá atualizar o preço do produto baseado na porcentagem informada anteriormente.   

Caso deseje gerenciar esta atualização de valor de maneira manual, basta selecionar a opção __"Não, vou alterar manualmente os valores dos produtos para esta tabela"__

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Observações internas 
Este campo se destina a observações. Estas informações serão visíveis apenas para os aministradores da plataforma.

---

<span class="faq h2">Dúvidas frequentes</span> <i class="fa fa-question-circle-o" aria-hidden="true"></i>

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Como saber se um produto teve seu preço atualizado na tabela?  
R: No botão __AÇÕES__, opção __EDITAR__, procure a aba __Produtos desta tabela__, clique no link de redirecionamento. Nesta nova tela será mostrado uma listagem de todos os produtos que tiveram seus valores alterado em função da tabela. Você poderá encontrar informações como o nome do produto, a variação, o preço normal e o preço de oferta.  

> Estes valores são os atualizados pela tabela, ou seja, já receberam o variação de valor aplicado pela tabela. Se desejado, ao clicar sobre o valor, o mesmo pode ser alterado.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Consigo exportar os valores desta tabela para conferir manualmente?  
R: Na lateral esquerda da tela, logo acima da listagem de produtos, se encontra o botão __Imprimir__. Através deste, poderá imprimir a listagem completa, com as mesmas informações que aparecem nesta tela.
