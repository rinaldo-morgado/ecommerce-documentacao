# Conheçendo o CMS - Catálogo

## Biblioteca de Cores

A biblioteca de cores é um complemento para o cadastro do produto. Nela você pode cadastrar as cores/estampas de seus produtos de maneira que possa utilizar em mais de um produto.  
Ao cadastrar as cores na biblioteca facilita a criação das variações do produto e também possibilita o q.commerce vincular uma imagem a uma cor (será explicado no cadastro de produto).

No canto superior da tela, você irá encontrar um botão de **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo**.
Ao carregar a página para o cadastro da cor, deverá ser preenchidos alguns campos:


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome da cor
Informe o nome da cor. Este nome será utilizado futuramente na criação das variações do produto, sendo assim, utilize um nome amigável.  
Ex.: Cor branca, nome da cor pode ser simplesmente "Branco".

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> RGB  
Informe o RGB de uma cor, ou clique no campo para poder selecionar com o *picker*.  
Ex.: Branco: #FFFFFF / Cinza: #4D4D4D / Preto: #000000.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Imagem  
Caso seu produto tenha uma estampa, não é possível definir em apenas uma cor. Para isso, pode ser enviado uma imagem quadrada de 32px x 32px.  
Esta imagem se comportará como uma cor, e será mostrado ao usuário como uma cor, e também poderá ser utilizada na criação da variação.

![screenshot_biblioteca-de-cores](assets/img/prints/catalog/color-lib/cadastro-cor.png "Cadastro de cor / estampa")

---

## Características 

O ecommerce disponibiliza a opção de criar características para seus produtos. Estes poderão ser utilizados como filtros pelo cliente no momento da compra. Exemplo: Se for uma roupa, pode ter uma característica de Tecido, Gênero, Estilo, entre outros. Caso seja um produto de tecnologia, pode ter como característica a Voltagem, Sistema Operacional,  Processador, entre outros.

As características serão associadas à categoria, ou seja, todos os produtos daquela categoria poderão receber algum valor no campo cadastrado.


---
## Categorias

A plataforma de ecommerce da QualityPress trabalha com dois níveis de categoria. Sendo assim você pode ter uma categoria principal, como por exemplo:
>Acessórios  (categoria "pai")  
> > Aneis  
> > Correntes  
> > Relógios  

>Informática
>>Estabilizadores  
>>Computadores  



---

### Editando uma categoria
Para editar uma categoria existente, na listagem de categorias, no lado direito da página, clique no botão __AÇÕES__ e então em __EDITAR__.
Ao carregar a página, realize as edições necessárias e salve ao final.

---

### Criando uma categoria
Para criar uma nova categoria clique no botão **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo** na parte superior da página.
Preencha o formuário referente a nova categoria que deseja cadastrar.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Informe o nome da Categoria
Informe o nome da categoria. Este nome será usado em todos os momentos para se referenciar a esta categoria. Lembrando que este campo é _case-sensitive_, ou seja, se você cadastrar o nome da categoria em caixa alta, ex.: COMPUTADORES, ela será mostrada sempre assim, em caixa alta.   

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Informe se esta categoria será uma subcategoria de:    
Caso a categoria que estiver criando, for uma categoria "filha" ou subcategoria, selecione a qual categoria ela será filha.  
Ex.: COMPUTADORES, subcategoria de Eletrônicos.
Em caso de ser uma categoria principal, basta não selecionar nenhuma categoria, ou selecionar a opção __--__.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Define a ordem desta categoria:
Define a ordenação que esta categoria irá aparecer, a ordenação é com relação às demais categorias do mesmo nível.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mostrar na Barra de Categorias? 
Na home de seu e-commerce, existe uma barra onde são listadas algumas categorias. Estas ficam logo abaixo da caixa de pesquisa e do logo de sua empresa.  
Ao selecionar __SIM__, a sua nova categoria será mostrada nesta barra.  
!!! error ""
    Somente as categorias do primeiro nível podem ser disponibilizadas na barra de categorias.

![screenshot-category-bar](assets/img/prints/catalog/category/category-bar.png "Barra de categorias")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mostrar na Página inicial? 
Na página inicial de seu e-commerce, logo abaixo dos banners (principal e vantagens caso tenha) são listadas algumas categorias com seus produtos.  
Ao selecionar __SIM__, automáticamente uma quantidade específica de produtos desta categoria será mostrada na página inicial da seguinte forma.  
![screenshot-display-home](assets/img/prints/catalog/category/display-home.png "Categorias na home")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mostrar no site?  
Esta é a opção para definir se esta categoria será mostrada ou não no site. Caso seja escolhido __NÃO__, esta categoria não será listada em nenhum lugar no e-commerce, apenas no CMS.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Banner  
Esta é uma opção de upload de um banner de categoria.  
Quando enviado uma imagem nas proporções indicadas (1140x114 pixels), esta será mostrada na página principal da categoria, logo acima dos produtos.
![screenshot-category-banner](assets/img/prints/catalog/category/banner-cat.png "Banner de categoria")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Foto de Menu  
A plataforma disponibiliza a opção de inserir uma imagem da categoria para aparecer na barra de categorias conforme print:
![screenshot-categoria-home](assets/img/prints/catalog/category/foto-menu.png "Foto no menu")


!!! type ""
    Caso não tenha produtos vinculados a esta categoria, ela não será exibida.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Características associadas a categoria  
Selecione as características que fazem parte desta categoria.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Salvando  
Após preencher todos os campos obrigatórios, salve sua nova categoria clicando em _SALVAR_.

---

## Comentários

No e-commerce seu cliente tem a possibilidade de fazer um comentário sobre o produto que ele comprou. Na página de Comentários em seu CMS você poderá ver todos os comentários realizados em seus produtos, tendo a opção de aprovar ou deletar o comentário.  
Em cada item da listagem de comentários, você terá as seguintes informações:

*  Cliente que enviou o comentário  
*  Título e o comentário  
*  O produto que recebeu este comentário  

![screenshot-comments](assets/img/prints/catalog/comments/comments.png "Comentário aprovado dentro do CMS")

Ao ser aprovado, o comentário será exibido na página de detalhes do produto que recebeu o comentário, possibilitando assim que outros clientes vejam e também possam comentar.

---

## Marcas

O e-commerce disponibiliza um cadastro de marcas.  
Estas marcas podem ter produtos vinculados e posteriormente pode-se filtrar os produtos por marca.  

Para cadstrar um nova marca, basta clicar em __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo__ no topo da página. Preencha os campos do cadastro informando o nome da marca e também o logo ou imagem de referência (155x144 pixels).  

Feito isso, no cadastro de produtos, poderá ser vinculado o produto a uma marca específica. Existe também a possibilidade de mostrar uma barra de marcas na página inicial do site. Esta barra de marcas, é um carrossel que se movimenta sozinho, assim mostrando todas as marcas cadastradas.

![screenshot-barra-de-marcas](assets/img/prints/catalog/brands/brands-bar.png "Barra de marcas")

---

## Ordenar variações
O e-commerce disponibiliza a opção de ordenar os atributos de suas variações.
Você encontrará uma listagem contendo os atributos cadastrados nos produtos, para cada atributo contém as variações cadastradas nele. Para ordenar as variações você deve clicar no nome do atributo para abrir a lista de variações, após abrir a lista, você deve clicar e arrastar a variação para a posição que você desejar.

![screenshot-attrib-order](assets/img/prints/catalog/attribs/attrib-order.png "Ordenação de variações por atributo")

---

## Produtos

Na tela de produtos é possível visualizar todos os produtos cadastrados na plataforma. É possível filtrar os produtos para mostrar apenas os esolhidos.

Para criar um novo produto, clique em __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo__.
Preencha os campos informando os dados do produto conforme desejado:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome do produto
Este item é indispensável pois será através deste que seu cliente irá identificar o produto no Q.Commerce. Evite abreviações e informações denecessárias.  
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Referência
Este deve ser único para cada produto, pode ser entendido também como o SKU de um produto. Esta referência o cliente poderá ver no Q.Commerce.  
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> EAN
O EAN é o código de barra do produto. Para entender mais, veja este [link](https://pt.wikipedia.org/wiki/EAN-13).

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Marca
Este campo serve para definir qual a marca do produto que está sendo cadastrado.   
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Gênero
Define a qual gênero é destinado aquele produto, esta informação será útil em integrações com marketplaces e também nos filtros.

Veja neste print, os passos já preenchidos acima:
![screenshot-produto-info](assets/img/prints/catalog/produtos/01.png "Informações do produto")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Dados para cálculo do frete
Estas informações será utilizadas para compor o preço de frete do produto quando for comprado.  
Deve ser informado os seguintes campos:

!!! warning ""
    Os dados informados abaixo deverão sempre seguir as medidas do produto em sua embalagem de envio. 

* Peso (gramas)    - O peso do produto na embalagem em gramas.
* Altura (cm)      - Altura do produto na embalagem em centímetros.
* Largura (cm)     - Largura do produto na embalagem em centímetros.
* Comprimento (cm) - Comprimento do produto na embalagem em centímetros.

Veja neste print, os passos já preenchidos acima:
![screenshot-infos](assets/img/prints/catalog/produtos/02.png "Informações de peso e medidas")

Quanto as dimensões e pesos aceitos pelo correios, são as regras:

* A soma das dimensões (A+L+C) não podem ultrapassar 200cm.
* O limite de cada dimensão (A+L+C) é de 105cm.  
* O mínimo de cada dimensão (A+L+C) é de: 
    - Comprimento: **16cm**
    - Largura:     **11cm**
    - Altura:      **02cm**
* O limite de peso é de __30kg__

Para saber mais, veja este [link](https://www.correios.com.br/para-voce/precisa-de-ajuda/limites-de-dimensoes-e-de-peso).  
Veja também sobre as [restrições de envio](https://www.correios.com.br/para-voce/precisa-de-ajuda/proibicoes-e-restricoes).  

Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tabela de medidas
Conforme pode ser visto na parte de [tabela de medidas](#tabela_de_medidas_1), além de ser possível incluir uma tabela de preço para vários produtos ao mesmo tempo, é possível também incluir uma tabela para um produto específico. Basta clicar em _Selecionar Imagem_ e realizar o envio.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tags de pesquisa
As tags servirão para auxiliar na busca dos produtos. Em algumas vezes, o consumidor pode buscar por algum termo que não esteja presente no nome do produto. Com as tags preenchidas, será possível adicionar outros termos, tais como, a marca, as categorias, as informações das variações, as referências e até nomes similares relacionados ao produto. Tudo isso irá auxiliar o consumido a encontrar o produto que procura.

Seguindo o mesmo exemplo do produto criado acima, veja o print abaixo:
![screenshot-tagpesquisa](assets/img/prints/catalog/produtos/03.png "Tags de pesquisa")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Estoque
A parte de estoque conta com dois campos numéricos. O primeiro campo de Estoque Atual se refere a quantidade do produto que se encontram em seu estoque real.
O campo de estoque mínimo serve como um "gatilho", quando aquele produto atingir o estoque mínimo, o administrador do Q.Commerce recebe um e-mail informando que o produto está com o estoque mínimo, possibilitando assim repor o estoque em tempo.  
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

Veja abaixo um exemplo:  
![screenshot-estoque](assets/img/prints/catalog/produtos/05.png "Estoque")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Preços
São disponibilizados dois campos de preço, um para o __Preço Normal__ do produto e o segundo campo de __Preço de oferta__ onde pode ser informado o preço promocional do produto.
Caso este não tenha promoção, basta deixar o campo com o valor 0 (zero). É possível também permitir ao Q.Commerce que gerencie o preço das variações, basta dar um _check_ na opção __Alterar o valor para todas as variações com base nos campos de preço acima__. Feito isso, ao alterar o valor normal do produto, as variações também terão o valor ajustado.  
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

Veja abaixo um exemplo:  
![screenshot-precos](assets/img/prints/catalog/produtos/06.png "Preços")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Categorias
Nesta parte é possível vincular o produto às categorias. É interessante também vincular o produto as categorias pais, desta forma, se o cliente seleciona a categoria pai (ou principal) ele ainda poderá ver o produto.

Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

Veja neste print, os passos já preenchidos acima:  
![screenshot-categoria](assets/img/prints/catalog/produtos/07.png "Categorias")




### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Descrição do produto
Nesta parte deverá ser informado todas as especificações do produto, tais como uma descrição do mesmo.  Essas informações são impressindiveis para passar confiabilidade em seu produto. Estes campos contam com um editor de texto que possibilita editar o texto conforme necessário.  
Atenção: Este é um dos itens indispensáveis para a visibilidade do produto para o cliente.

Veja no print abaixo um exemplo:
![screenshot-descricao](assets/img/prints/catalog/produtos/04.png "Descrição do produto")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Publicação
Finalizando o cadastro das informações do produto, você terá três opções com relação a publicação do mesmo:

*   Publicado? - Opção SIM/NÃO para definir se o produto será publicado ou não. Caso selecionado __não__ o produto ficará disponível apenas no administrador tendo a possíbilidade de publicar mais tarde.
*   Destaque? - Opção SIM/NÃO para definir se o produto será destaque ou não. Caso selecionado __sim__ o produto irá aparecer na página inicial, logo abaixo do banner principal. Para esta funcionalidade, a categoria em que o produto está vinculado deve estar habilitada para aparecer na homepage. Veja como em [Mostrar na Página Inicial](#mostrar_na_pagina_inicial).
*   SEO Automático? - Opção SIM/NÃO para definir se o SEO deste produto será gerado automaticamente. Para entender mais sobre o que é o SEO, veja em [Search Engine Optimization](#seo_search_engine_optimization).

Veja no print abaixo um exemplo:
![screenshot-publicacao](assets/img/prints/catalog/produtos/08.png "Publicação do produto")

---


Finalizado a primeira parte do cadastro do produto, está na hora de configurar os demais itens que vão compor o produto.
Na tela de edição do produto, selecione a aba __FOTOS__ conforme o print abaixo:  
![screenshot-abafoto](assets/img/prints/catalog/produtos/09.png "Aba de fotos")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Fotos
Para inserir uma foto ao produto, clique no botão **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar arquivos**. Selecione as imagens que serão utilizadas no produto, é possível selecionar mais de uma imagem por vez, e posteriormente é permitido ordenar as mesmas. Ao ter todas as imagens selecionadas, clique em <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> __Iniciar Upload__ e todas as imagens serão enviadas.

Conforme print abaixo, cada produto terá o campo de ordem, que definirá a ordem em que ele será mostrado na tela do produto, a legenda que pode ser utilizada para diferenciação das imagens, e tendo também a possíbilidade de vincular a foto a uma cor. Desta forma, quando o cliente selecionar a cor, será mostrado essa imagem.  
![screenshot-imagens](assets/img/prints/catalog/produtos/10.png "Imagens")


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Atributos  
Atributos de produtos são as opções na qual o cliente poderá selecionar na página de detalhes do produto.
Por exemplo. No ramo vestuário, estas opções podem ser tamanho e/ou cor, no ramo de eletrônicos pode ser voltagem.

Obs.: Uma vez definidos os atributos, você só poderá adicionar ou remover atributos iniciando o processo novamente. Isto poderá ser feito clicando no botão "Reiniciar" no final desta página. 

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Variações

Uma vez criados os atributos é possível criar as variações do produto baseado nos atributos. Ao clicar em Inserir variações, preencha os campos dos atributos com quantos forem necessários, basta separar com vírgula ou _tab_. É possível também escolher o valor das variações tais como o preço promocional, o estoque atual, a quantidade mínima e a opção de liberar ou não para a venda. Feito isso, clique em __Gerar variações__. 

Veja um exemplo no print abaixo: 

![screenshot-variacao](assets/img/prints/catalog/produtos/11.png "Variações")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Características

Conforme já criado anteriormente no módulo de [Características](#caracteristicas), insira nesta tela as informações de características do produto. Estes itens servirão como filtro. 

Preze sempre pela padronização das características, exemplo: Se o produto tem uma voltagem, use sempre 220V ou 110V, e não varie como "220 v" ou "220v". Assim, todos os produtos que contém a característica __voltagem__ estarão com a mesma informação.

Veja mais neste print abaixo:

![screenshot-caracteristicas](assets/img/prints/catalog/produtos/12.png "Características")

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Associações

O E-commerce trabalha com a possibilidade de relacionar produtos manualmente. No botão Nova Associação, informe o nome da associação, este aparecerá na página do produto, escolha o tipo de associação, podendo variar entre:

#### Produtos Relacionados  
São produtos que possuam qualquer tipo relação com o produto em questão, seja através de complemento ou de semelhança.

#### Venda Cruzada  
São produtos que poderão ser adicionados ao carrinho juntamente com o produto em questão. A relação pode ser através de complementos ao produto principal.

#### Venda Conjunta   
São produtos que poderão ser adicionados ao carrinho juntamente com o produto em questão. Apenas 1 conjunto aparecerá na tela de detalhes do produto através de um botão ao lado do botão comprar.

Defina a ordem em que esta associação irá aparecer na página e marque se disponível ou não. Feito isso, acesse a aba de __Associações Criadas__,  clique no link de __Ver produtos associados__. Ente na aba __Associar novos produtos__ e selecione os produtos que serão vinculados a esta associação.

Veja mais detalhes nos prints abaixo:

* Criação de uma associação:
![screenshot-assoc](assets/img/prints/catalog/produtos/13.png )

* Vinculando produtos a uma associação:
![screenshot-assocprod](assets/img/prints/catalog/produtos/14.png )


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Shoppings

O E-commerce possibilita vincular os produtos em outras lojas virtuais, como o Google Shopping, Buscapé Company e também o UOL Shopping.
Utilizando a url encontrada na [Dashboard](/#xml_google_shopping_buscape_company) você pode realizar o vínculo com cada um deles.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Shoppings - Google Shopping
Para disponibilizar o produto no Google Shopping, preencha o formulário conforme o produto. Selecione se ele está ativo ou não para integração, selecione a categoria do Google na qual o produto se enquadra (Atenção: Estas categorias não são as mesmas cadastradas no site), selecione então a categoria que o mesmo pertence no E-commerce, a faixa etária, gênero, condição (novo ou usado), selecione sobre o uso das imagens, informe se o produto é para maiores de 18 ou não. Caso seu produto seja da categoria __Vestuário e acessórios__, informe o tamanho e cor. Feito isso, o produto estará integrado no XML, basta configurar o XML em sua conta do Google Shopping para vender os produtos através do Google.

Veja mais no print abaixo:
![screenshot-gshopping](assets/img/prints/catalog/produtos/15.png )

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Shoppings - Buscapé Company
Diferente do cadastro do Google Shopping, para configurar o produto no Buscapé Company, basta selecionar se ele está ativo ou não. Integrando a URL do XML que está na [Dashboard](/#xml_google_shopping_buscape_company), seu produto já será comercializado através do Buscapé.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Shoppings - UOL Shopping
Semelhante ao Buscapé Company, basta apenas ativar o produto para que este seja comercializado pelo UOL.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Integrações
Esta aba se destina a configuração dos produtos nas integrações do Plugg.To e Mercado Livre.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Integrações - Plugg.To
Para integrar o produto ao Plugg.to, basta clicar no botão __INTEGRAR__. Lembrando que este apenas funcionará caso a integração com o Plugg.to já esteja configurada, veja mais em [Configurar o Plugg.to](pluggto/#como_configurar_a_integracao_entre_a_minha_plataforma_e_o_pluggto).

---

## Tabela de Medidas
O cadastro de tabela de medidas ocorre em duas etapas, na primeira, através dos filtros laterais escolha os produtos que receberão a nova tabela de medidas e clique em Próximo Passo. No segundo passo, insira a imagem que servirá de tabela de medidas.

!!! warning ""
    Caso o produto selecionado já possua uma tabela de medida, a mesma será sobrescrita pela nova.