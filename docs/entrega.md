# Conheçendo o CMS - Formas de Entrega

## Correios

Esta página se destina a configuração da forma de entrega pelos Correios e Sedex.


### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar PAC
Define se o PAC será habilitado como meio de entrega.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar Sedex
Define se o SEDEX será habilitado como meio de entrega.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar E-Sedex
Define se o E-SEDEX será habilitado como meio de entrega.

!!! error ""
    Para ter o e-sedex habilitado, é necessário ter o código e a senha do contrato com os correios devidamente preenchidos.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Código do Contrato
Define o código de contrato junto aos correios.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Senha do Contrato
Define a senha do contrato junto aos correios.

---

## Retirada na Loja

Caso esta seja uma opção para a sua empresa, é possível cadastrar pontos de retirada em loja.
É possível cadastrar mais de uma loja para retirada.

### Cadastrando loja de retirada
Para cadastrar uma nova loja de retirada, clique no botão **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo** no topo direito do site.
Preencha os campos com as informações necessárias:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome da Loja
Informe o nome da loja que deverá ser feito a retirada, ex.: Loja do Centro, Loja Bairro Capão.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Disponível
Define se esta loja de retirada estará disponível ou não para os usuários.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Telefone
Deverá ser informado o telefone de contato da loja em questão. Este detalhe pode ser importante caso o cliente não consiga encontrar a loja ou tenha algum problema, ele poderá sempre entrar em contato com sua empresa.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Endereço
Campo destinado a informar o endereço da loja que irá disponibilizar o produto. Assim como o telefone, este campo deve ser preenchido com o máximo de detalhes possível.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor a ser cobrado na retirada (R$)
Define o valor que será cobrado na loja no momento da retirada. Caso não for realizado nenhuma cobrança, basta deixar o campo com o valor zero.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Prazo de retirada (dias úteis)
Define a quantidade de dias que o cliente tem para poder retirar o produto em sua loja. Esta quantidade é dada em dias úteis.

---

## Transportadoras
Na plataforma do Q.commerce é possível trabalhar com transportadoras. O módulo consiste em faixas de região, utilizando o CEP inicial e CEP final, e para cada região cadastrada, a opção de faixas de peso ilimitadas.

Para o cadastro de uma região, clique no botão **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo**, serão necessárias as seguintes informações:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome da Região
Define o nome da região a qual a faixa de CEP irá corresponder. Ex.: Estado de Santa Catarina, Grande São Paulo, Médio vale, Sul, Sudeste.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CEP Inicial
Define o CEP inicial da contagem, todo CEP a partir do que for cadastrado neste campo, receberá os valores desta faixa.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CEP Final
Define o CEP inicial da contagem, todo CEP que estiver no escopo entre o inicial e o final estará passível de receber o preço das faixas de preço e etc.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Observações
Este campo serve simplesmente para observações, as mesmas são mostradas apenas para o administrador, ou seja, seu cliente não terá acesso a esta observação.

---

Após o cadastro de regiões, que foi feito acima, você poderá cadastrar faixas de peso para cada região.  
Para o cadastro de uma faixa de peso, clique no botão **<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Novo** e preencha as iformações solicitadas.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Peso (a partir de n gramas)
Define a partir de qual peso o pedido se enquadra. Ex.: A partir de 1000 gramas, cobrar R$X,xx. A partir de 2000 gramas, cobrar R$Y,yy.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Prazo de entrega (em dias)
Define o prazo da entrega em dias. Ex.: Entrega em até 7 dias. (Obs.: São contados dias corridos e não úteis.)

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tipo de cobrança
* <span class="bannerkind">Preço Fixo</span>: O valor do frete é baseado em uma tabela de intervalos de peso com preço fixo por intervalo. Ex.: a partir de 1000 gramas cobrar R$ 25,00, a partir de 1500 gramas cobrar R$ 30,00.
* <span class="bannerkind">Porcentagem</span>: O valor do frete é baseado em uma tabela de intervalos de peso, com porcentagem com base no valor do pedido, por intervalo. Ex.: a partir de 10kg cobrar 3% do valor do pedido, a partir de 20kg cobrar 5% do valor do pedido.
* <span class="bannerkind">Valor por Kg</span>: O cálculo do frete é efetuado pela multiplicação do peso do produto pelo valor do kg cobrado, baseado em uma tabela de intervalo de pesos, ex: faixa a partir de 10kg cobrar R$ 1,00 por kg, faixa a partir de 20kg cobrar R$ 1,50 por kg.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor
Define o valor que será usado para calcular o preço do frete. Seja este valor em R$ ou %.

---

<span class="faq h2">Dúvidas frequentes</span> <i class="fa fa-question-circle-o" aria-hidden="true"></i>

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Tenho faixas de peso iguais para alguns estados, terei que cadastrar duas vezes a mesma faixa em regiões diferentes?  
R: Não, ao acessar a parte de faixas de peso, você encontrará um botão **<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> Importar**. Através dele, você poderá selecionar faixas de peso de outra região e com apenas um clique importar para a sua nova região.

> "Mas nem todas as minhas faixas de peso são iguais para a nova região!" Sem problemas, importe e depois edite ou exclua.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Trabalho com apenas uma transportadora específica, e eles disponibilizam uma API de integração, como posso fazer para integrar na minha plataforma?  
R: Procure o seu comercial responsável na QualityPress, ele irá lhe auxiliar no que for preciso.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  Não sei as faixas de CEP do Brasil, onde posso encontrar isso?
R: O Correios disponibiliza uma consulta as faixas de CEP. Acesse através deste [link](http://www.buscacep.correios.com.br/sistemas/buscacep/buscaFaixaCep.cfm).
