# Conheçendo o CMS - Conteúdos

## Banners

Na página de banners, poderá ser encontrado todos os banners cadastrados na plataforma. Na listagem, encontrará informações como o tipo do banner, título, ordem e se o mesmo se encontra ativo.

### Cadastrando um novo banner

Para adicionar um novo banner, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__ no canto direito da tela. Ao carregar a página, encontrará os campos necessários para o envio do banner. São estes:

__Título__: Título do banner.  
__Ordem__: Denfine em qual posição este banner será mostrado.  
__Ativo__: Define se o banner está ativo para ser mostrado ou não.  
__Tipo__:

* <span class="bannerkind">Banner principal</span>: Banner exibido na home do site, logo abaixo a barra de categorias.  
* <span class="bannerkind">Barra de vantagens</span>: Banner exibido logo abaixo do banner principal, destinado a expor vantagens da loja virtual, como: Frete grátis para algum estado, quantidade de parcelas, trocas e etc.  
* <span class="bannerkind">Carrinho de compras</span>: Banner semelhante ao de vantagens, porém exibido na tela do carrinho.    

__Upload de imagens__: No upload de imagens, poderá encontrar 3 tamanhos de imagens diferentes. Seguindo a tabela de dimensões abaixo, envie os banners de acordo com cada tamanho.

|                    | Celulares (<768px)     | Tablets (≥768px)      | Notebooks (≥992px)    |  Desktops (≥1200px)    |
|-----------------------    |:-------------------------:    |:--------------------------:    |:-------------------------:   |:----------------------------------:   |
| Banner Principal    |   <i class="fa fa-times-circle" aria-hidden="true"></i>   |   768x384 (2:1)     |        1200x400 (3:1)     |        1920x480 (4:1)        |
|Banner de Vantagem|<i class="fa fa-times-circle"</i>|<i class="fa fa-times-circle" ></i> | 1140x89 |   <i class="fa fa-times-circle" aria-hidden="true"></i>    |
| Carrinho de compras     |     720x89 (2:1)     |        940x89 (3:1)       |     1140x89              |           <i class="fa fa-times-circle" aria-hidden="true"></i>              |


__Link__: Link do banner, que quando o usuário clicar no banner será redirecionado.  
__Abrir em__: Define se o link cadastrado acima será aberto na mesma aba ou em uma nova aba. 

---

## Contato

Na tela de conteúdo poderá ser encontrado uma listagem de parâmetros que podem ser substituidos pelos dados de sua empresa. São estes:

### <i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp 
Define o número do WhatsApp.

### <i class="fa fa-envelope" aria-hidden="true"></i> E-mail de Contato  
Define o e-mail que será mostrado na página de contato.

### <i class="fa fa-phone" aria-hidden="true"></i> Telefone de Contato  
Define o número do telefone para os cliente que desejarem entrar em contato via telefonema.  

### <i class="fa fa-map" aria-hidden="true"></i> Google Maps - Descrição
Define o conteúdo que aparecerá no box do mapa do google maps na página de contato.  

### <i class="fa fa-location-arrow" aria-hidden="true"></i> Google Maps - Latitude
Define a latitude para o google maps.

### <i class="fa fa-location-arrow" aria-hidden="true"></i> Google Maps - Longitude
Define a longitude para o google maps.

---

##  Galeria de imagens

A galeria de imagens funciona como um álbum de imagens. As imagens cadastradas nesta galeria podem ser utilizadas em mais de um local o mesmo tempo. Na página do módulo, poderá encontrar uma listagem de galerias criadas contendo nome e a quantidade de imagens.

### Cadastrando nova Galeria de Imagens
Para cadastrar uma nova galeria de imagens, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__. Ao carregar a página, esta estará dividida em duas colunas, a __Galeria__ e __Imagens__.
Na parte da Galeria, informe o nome e também uma breve descrição da galeria e clique em salvar.

!!! type ""
    Somente apos a criação da galeria será possível vincular imagens.

Após salvar a galeria, clique no link <a>ver imagens</a>. Ao carregar a tela com as informações, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar Arquivos...__. Selecione as imagens que farão parte de sua galeria, e clique em __Iniciar Upload__. Ao final do carregamento, as imagens estarão vinculados a esta galeria.

---

## Perguntas Frequentes

A página de perguntas frequentes (FAQ - Frequently Asked Questions) serve para o cadastro e controle das perguntas realizadas no Q.commerce. Quando um usuário envia uma pergunta, esta fica disponibilizada nesta listagem da página de Perguntas Frequentes no CMS, a partir deste momento, as perguntas poderão ser respondidas e disponibilizadas para os demais usuários também na página de __Perguntas Frequentes__.

Para responder uma pergunta feita, clique no botão __AÇÕES__, opção __EDITAR__.
Nesta página, poderá encontrar os seguintes campos para edição:

### <i class="fa fa-angle-right" aria-hidden="true"></i>  Pergunta:
Pergunta feita pelo usuário. É possível editar a pergunta para correção de erros, melhoria de linguagem, conforme identificar necessário.

### <i class="fa fa-angle-right" aria-hidden="true"></i>  Resposta:
Campo de texto para inserir a resposta da pergunta do usuário.

### <i class="fa fa-angle-right" aria-hidden="true"></i>  Ordem:
Define a ordem em que a pergunta irá aparecer no Q.commerce. Recomenda-se ordenar por importância, ou pelo item que sempre gera dúvidas aos usuários.

### <i class="fa fa-angle-right" aria-hidden="true"></i>  Ativo:
Define se a pergunta estará ativa ou não para os usuários.

### Adicionando uma pergunta
Caso queira adicionar uma pergunta através do CMS, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__. Os mesmos campos citados acima serão mostrados, basta preenche-los da maneira correta e disponibilizar no site.

---

## Páginas e Blocos