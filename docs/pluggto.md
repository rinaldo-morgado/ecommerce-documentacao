# Conheçendo o CMS - Integração com Plugg.to

## Como configurar a integração entre a minha plataforma e o Plugg.to

Para configurar o Plugg.To para integrar com sua plataforma, basta preencher os campos solicitados:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> API SECRET  
Você poderá encontrar esta informação acessando a seguinte página do Plugg.To - [https://core.plugg.to/users/profile](https://core.plugg.to/users/profile), depois em __Dados de acesso à API__, e então em __API Secret__. Copie o conteúdo deste campo e cole neste campo da plataforma.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> API USER
Para coletar o _user_ (usuário), acsso o mesmo link do campo acima, [https://core.plugg.to/users/profile](https://core.plugg.to/users/profile), novamente em __Dados de acesso à API__, e então em __API User__.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Client Secret
Para coletar o _Client Secret_, acesso a página [https://core.plugg.to/users/integration](https://core.plugg.to/users/integration), vá até __Plugins__ > __Q.Commerce__ e então colete a chave do _Client Secret_.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Client ID
Para coletar a ID do cliente acesso o mesmo caminho onde coletou a _Cliente Secret_ e colete a __Client ID__.

## Funcionamento e regras


### Produtos

Você terá um menu chamado Plugg.to na aba Integrações dentro da página de edição de um produto no Q.CMS.  
Lá você poderá habilitar o produto para que o e-commerce o envie ao Plugg.to.  
Ao tentar habilitar, o e-commerce efetuará uma validação no produto para conferir se todos os campos
estão devidamente preenchidos. Caso haver alguma inconsitência, uma mensagem será apresentada descrevendo
o problema e como resolver.

Após habilitar alguns produtos, através de uma tarefa agendada, o e-commerce enviará os produtos ao 
Plugg.to. Esta tarefa normalmente é executada a cada 10 minutos. Mas não se preocupe, você conseguirá
ver o status da integração para saber se ele foi integrado ou não.

É muito importante que você tenha definido os SKU (Referência) dos produtos para que não haja alteração
futuramente. Isso é importante porque o Plugg.to associa o produto ao e-commerce através de seu SKU.
Sendo assim, ao ler os itens de um pedido do Plugg.to, todos os SKUs devem estar cadastrados no e-commerce,
pois do contrário, o pedido será ignorado.

### Pedidos

A cada 15 minutos o e-commerce solicita ao Plugg.to, através de uma tarefa agendada, todos os pedidos 
que ainda não foram integrados. O e-commerce avalia cada um deles e com base nas regras abaixo inicia
a importação.  
- Todos os SKUs devem estar presentes no e-commerce;
- O pagamento deve estar aprovado (responsabilidade do marketplace);

Os pedidos que não tiverem o pagamento aprovado, o e-commerce aguardará até que eles sejam aprovados
ou cancelados.  
Os pedidos que possírem algum SKU que não esteja presente no e-commerce serão ignorados e não serão importados.

#### Forma de pagamento e frete

A forma de pagamento e o frete serão denominados como Plugg.to.

#### Status do pedido

A cada avanço de status de um pedido importado do Plugg.to, o e-commerce atualizará o pedido no plugg.to 
através de uma rotina agendada que executa de 15 em 15 minutos.
O próprio Plugg.to fará a atualização no marketplace.

#### Código de rastreio

Ao salvar um código de rastreio em um pedido importado do plugg.to, o e-commerce atualizará este pedido 
automaticamente no plugg.to através de uma rotina agendada que executa de 15 em 15 minutos.

### Clientes

Os clientes são importados para a plataforma, mas mesmo assim, se algum cliente que comprou através de
um marketplace quiser comprar no e-commerce, terá que efetuar o cadastro completo.  
O que ocorrerá é que o e-commerce verificará através do CPF ou CNPJ se o cliente já está cadastrado
na base de dados e apenas o atualizará caso esteja, desde que ele tenha sido importado do Plugg.to.  
Os motivos que impedem o cliente de se autenticar são:
- Os marketplaces não enviam o e-mail real do cliente;
- A senha é criptografada e terá que ser definida pelo cliente;
