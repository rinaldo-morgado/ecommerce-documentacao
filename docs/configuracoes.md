# Conheçendo o CMS - Configurações

## Administração

Na administração pode ser alterado algumas informações básicas do qcommerce, tais como:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> E-mail de contato  
Define o e-mail principal do sistema. Este e-mail receberá as notificações enviadas pelo sistema, tais como contatos, dúvidas e novos pedidos.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CEP de origem  
Define o CEP de origem da mercadoria para o cálculo do frete.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar modo __"Em manutenção"__  
Define se o site ficará em modo de manutenção e não ficará disponível para os clientes. Estes, quando acessarem o site, verão a seguinte [página](http://www.qapi.com.br/aguarde/index.php).

---

## Avaliação de Produtos

Na página de avaliação de produtos poderão ser feitas configurações sobre a parte de avaliações no site, que são estas:  

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Enviar e-mail de avaliação na finalização do pedido?
Define se o sistema enviará um e-mail convidando o cliente a efetuar avaliações referentes ao produto na finalização do pedido.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Conteúdo padrão do e-mail de avaliação
Define um conteúdo padrão que irá em todos os e-mails enviados ao cliente relacionados ao convite de avaliação. Este conteúdo poderá ser utilizado para o envio de promoções por exemplo.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Assunto padrão do e-mail de avaliação
Define o assunto do e-mail de avaliação que será enviado para o cliente.

---

## Banners

Nesta página poderá ser configurado a largura do banner, se ele será _boxed_ [^1] , ou se ele será __full-width__.
Ou seja, define se o banner principal do site ocupará a largura do monitor ou se terá tamanho limitado na tela.


---

## Boleto

Nesta página poderá ser configurado:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Porcentagem de desconto para pagamento via boleto
Define a porcentagem de desconto para pagamento à vista com boleto bancário.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo para compras com boleto 
Define o valor mínimo de compra para pagamentos por boleto bancário.

---

## Catálogo

Na página de configurações do catálogo, poderá encontrar as seguintes alterações:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mostrar variação por grade  
Define o tipo de layout para a variação nos detalhes do produto. Este pode ser __por caixa de seleção__, __por grade__ ou por __ambos__. Obs.: Somente para produtos com 2 variações.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Modo de ordenação das categorias  
Define o modo de ordenação das categorias. Quando selecionado __ordem__, após salvar uma categoria, o sistema reorganizará suas categorias por ordem e depois por nome. Se selecionado __nome__, o sistema organiza apenas alfabéticamente.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Proporção das imagens de produtos    
Define a proporção das dimensões das imagens dos produtos. __3:4__ recomenda-se o cadastro de imagens 768x1024. __4:3__ recomenda-se o cadastro de imagens 1024x768 e __1:1__ recomenda-se o cadastro de imagens quadradas 1000x1000.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar aviso quando o estoque atingir o mínimo configurado?   
O estoque mínimo serve apenas como um aviso ao administrador. Ao ser definido no cadastro de produtos, assim que algum produto atingir o valor do estoque mínimo, o sistema enviará um e-mail ao administrador informando-o que o estoque do produto atingiu o estoque mínimo configurado. Cada produto (ou variação) possui a sua configuração.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Número máximo de subcategorias a ser exibido no menu de "Todas as Categorias"  
Define o número máximo de subcategorias que serão exibidas antes de adicionar o link "veja mais".

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Selecionar variação automaticamente  
Define como o e-commerce se comportará com relação à seleção automática de variação na página de detalhes do produto. Podendo escolher entre:  

* Não selecionar nenhuma opção automaticamente  
* Selecionar a primeira variação disponível  
* Selecionar a variação mais vendida  
* Selecionar a variação padrão configurada  

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mostrar página de marcas?  
Mostra ou oculta o link para a página de marcas.Em caso de marca própria, não é necessário ter um link para listar.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar avise-me quando disponível?   
Define se o haverá esta opção para o consumidor para produtos que não estiverem disponíveis.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Disposição dos produtos na página inicial  
Define o comportamento do layout dos produtos na página inicial do e-commerce. É possível agrupar através de um carousel ou através de uma lista corrida.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Limite de produtos por categoria na página inicial  
Define a quantidade limite de produtos por categoria quando na disposição de carrossel (Recomenda-se 8 para não sobrecarregar o site). 

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar consulta de frete na página de detalhes do produto  
Define se a consulta de frete na página de detalhes do produto estará visível.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Redirecionar cliente ao carrinho de compras?  
Define se o consumidor será ou não redirecionado ao carrinho de compras após adicionar um produto no carrinho pela página de detalhes do produto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar descrição abaixo do nome na tela de detalhes do produto.  
Define se será ou não disponibilizado a descrição resumida do produto logo abaixo do nome na tela de detalhes do produto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Disponibilizar o botão comprar na listagem de produtos?  
Define se será ou não disponibilizado o botão comprar na listagem dos produtos.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Disponibilizar o campo "Quantidade" ao lado do botão comprar na listagem de produtos?  
Define se será disponibilizado o campo "Quantidade" ao lado do botão comprar na listagem de produtos. Obs.: A opção de mostrar o botão comprar na listagem deve estar habilitado.

---

## Clientes

Na página de configurações dos clientes, poderá encontrar as seguintes configurações:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mensagem do e-mail para clientes aprovados  
Define a mensagem que o cliente receberá de boas vindas caso seu cadastro esteja aprovado.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mensagem do e-mail para clientes pendente de aprovação  
Define a mensagem que o cliente receberá de boas vindas caso seu cadastro tiver que passar por uma avaliação.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mensagem do e-mail para clientes em caso de reprovação de cadastro  
Define a mensagem que o cliente receberá caso seu cadastro seja reprovado pela central. O motivo preenchido no momento da reprovação irá abaixo deste texto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Enviar a descrição do motivo em caso de reprovação de cadastro ao cliente?  
Define se o cliente receberá no e-mail de reprovação o motivo na qual foi definido para a reprovação do seu cadastro.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Permitir o cadastro de...  
Define os tipos de cadastros que o sistema permitirá cadastrar-se:

* Pessoa Física e Jurídica  
* Somente Pessoa Física  
* Somente Pessoa Jurídica  

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Liberação de aprovação automática  
Define se os clientes que se cadastrarem receberão a aprovação do cadastro automaticamente.  

* Nenhum  
- Pessoa Física e Jurídica  
- Somente Pessoa Física  
- Somente Pessoa Jurídica  

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo de compra para Pessoa Física  
Define o valor mínimo para compras para cadastros do tipo pessoa física.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Valor mínimo de compra para Pessoa Jurídica  
Define o valor mínimo para compras para cadastros do tipo pessoa jurídica.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Quem pode visualizar os preços dos produtos?  
Define como será a permissão de visualização de preços dos produtos, sendo __todos__ ou __somente pessoas autenticadas__.

___

## Dados da Empresa

Nesta página serão definidas as configurações sobre a empresa, tais como:

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Inscrição Estadual  
Define a inscrição estadual da empresa. Dado obrigatório no rodapé do Q.Commerce conforme normas e leis[^2] para comércio eletrônico.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Razão Social  
Define a razão social da empresa. Dado obrigatório no rodapé do Q.Commerce conforme novas normas e leis[^2] para comércio eletrônico.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> CNPJ  
Define o CNPJ da empresa. Dado obrigatório no rodapé do Q.COMMERCE conforme novas normas e leis[^2] para comércio eletrônico.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Endereço  
Define o endereço da empresa. Dado obrigatório no rodapé do Q.COMMERCE conforme novas normas e leis[^2] para comércio eletrônico.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Nome Fantasia  
Define o nome fantasia da empresa.


---

## Faturamento Direto

Página de configurações da forma de pagamento por faturamento direto.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Habilitar Opção de Faturamento Direto  
Define se o pagamento via faturamento direto está habilitado ou não.

### <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Mensagem da tela de pagamento  
Define a mensagem que aparecerá na aba de faturamento direto na tela de pagamento do Q.Commerce

---

## Perguntas Frequentes - FAQ

Configuração para disponibilizar link para o módulo de perguntas frequentes no topo e no rodapé do Q.Commerce.

---

## Prazo adicional no frete
Este módulo permite criar faixas de data em que o prazo normal de entrega receberá alguns dias a mais conforma cadastrado.
Basta cadastrar uma nova data e escolher a quantidade de dias adicionais e salvar. Quando nas datas escolhidas, o ecommerce irá acrescentar os dias informados ao prazo de entrega.

---

## Tabela de redirecionamentos  
O intuito deste módulo é guardar o histórico de alterações das urls de produtos, categorias e marcas para que a página não perca seu ranking nos buscadores.

Ex.: Temos a URL ```.../produtos/meu-produto.``` Ele está muito bem no ranking de pesquisas. Caso a url dele mudar para ``.../produtos/nosso-produto``, a tabela de redirecionamentos fará com que todos que acessarem a url antiga sejam direcionados para a mais recente. Assim os buscadores recebem a informação de que a URL foi apenas alterada. Desta forma, ela não perderá seu ranking. 

Caso ela simplesmente não exista mais, ela cairá no ranking das pesquisas.



[^1]: Aproximadamente 1140px de largura. Veja o cadastro dos banners para ter uma ideia melhor das dimensões.
[^2]: [BRASIL. DECRETO Nº 7.962, DE 15 DE MARÇO DE 2013](http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2013/Decreto/D7962.htm). 
    > Art. 2º  Os sítios eletrônicos ou demais meios eletrônicos utilizados para oferta ou conclusão de contrato de consumo devem disponibilizar, em local de destaque e de fácil visualização, as seguintes informações: I - nome empresarial e número de inscrição do fornecedor, quando houver, no Cadastro Nacional de Pessoas Físicas ou no Cadastro Nacional de Pessoas Jurídicas do Ministério da Fazenda; II - endereço físico e eletrônico, e demais informações necessárias para sua localização e contato;  