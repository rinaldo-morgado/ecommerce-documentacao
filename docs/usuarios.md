# Conheçendo o CMS - Usuários

## Grupos

O sistema de permissão de usuários do Q.Commerce se dá através do gerenciamento de grupos de usuários.
Em cada grupo é possível definir quais partes do ecommerce o usuário administrativo terá acesso.

Clicando no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__ é possível criar um novo grupo de usuários.
Na tela de cadastro, informe o nome do grupo e marque as áreas do administrativo a qual este grupo terá acesso.
Após selecionar as áreas, aperte Salvar.

Exemplo:

__Nome do grupo__: _Marketing_  

Permissão de acesso para as seguintes áreas:  
> Clientes  
> Banners  
> Contato  
> FAQ  
> Galeria de Imagens  
> Páginas e blocos  
> Mídias sociais  
> Newsletter  
> SEO  
> Relatório de aniversariantes  

Desta forma, o usuário que for criado posteriormente e for marcado no grupo de marketing terá acesso apenas as áreas citadas acima.


---

## Usuários

Esta área no Q.Commerce destina-se a criação e ediçãos dos usuários administrativos da plataforma. Os usuários aqui cadastrados terão completo acesso a parte administrativa da plataforma, de acordo com o grupo a ele vinculado. 

Para criar um novo usuário, clique no botão __<i class="fa fa-plus-circle" aria-hidden="true"></i>Adicionar novo__.
Preencha o cadastro conforme dados solicitados:

*   Nome: nome do novo usuário.
*   E-mail: e-mail do novo usuário.
*   Login: login do novo usuário, este será o método de acessar o painel.
*   Senha: senha intransferível para acesso.
*   Grupo: grupo de usuários a qual este irá pertencer.

Feito isso, basta salvar e imediatamente, seu novo usário poderá ter acesso ao admin da plataforma.
