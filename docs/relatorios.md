# Conheçendo o CMS - Relatórios

## Aniversariantes
No relatório de aniversariantes é possível saber quais de seus cleintes estão fazendo aniversário.
Este relatório se torna útil nas criações de campanhas relacionadas ao aniversário do cleinte, ou até mesmo o envio de email parabenizando o cliente.

O relatório conta com as colunas de data de aniversário, nome, e-mail e telefone.
Existe também de filtrar por mês e também de exportar o relatório. 

Veja mais no print abaixo:

![screenshot_report_aniversário](assets/img/prints/reports/aniver.png "Relatório de aniversariantes")

---

## Avise-me quando disponível
Umas das funcionalidades do Q.Commerce é salvar um breve contato de seu cliente quando o produto não está disponível. 
Tem-se então o Avise-me quando disponível, ao preencher os campos para ser avisado da disponibilidade do produto, os dados de seu cliente ficam salvos no CMS.

No relatório de Avise-me no CMS, é possível visualizar a data que foi solicitado, o nome do cliente, o e-mail, telefone e o produto que foi solicitado o aviso.
O relatório conta com opções de filtros por data, nome e referência do produto. Além da opção de exportar, padrão para todos os relatórios.

Veja mais no print abaixo:

![screenshot_report_aniversário](assets/img/prints/reports/aviseme.png "Relatório de Avise-me quando disponível")

---

## Estoque
No relatório de estoque é possível ter uma ideia do estoque de seu ecommerce. Neste relatório é possível visualizar o nome do produto (com link para o cadastro do mesmo), a quantidade disponível, a quantidade reservada[^1] , total de produtos no estoque (Disponíveis + Reservados) e a quantidade mínima do estoque.
[^1]: Reservados: Produtos que foram comprados que ainda não tiveram o pagamento do pedido confirmado.

Numa primeira instância são mostrado os produtos que estão sem estoque, esses são marcados em vermelho. Logo após são mostrados os produtos que estão com o estoque abaixo do limite, estes em amarelo. E por fim, os demais produtos que estão com estoque positivo.

Existe a possibilidade de filtros como também a opção de ordenar os campos da tabela, exemplo: Produtos com mais estoque/Produtos com menos estoque. Além da possibilidade também de imprimir o relatório.

Veja mais no print abaixo:
![screenshot_report_estoque](assets/img/prints/reports/estoque.png "Relatório de Estoque")

---

## Formas de Pagamento

No relatório de formas de pagamento é possível ter uma visão das vendas no site e por quais formas de pagamento. Através dos filtros de tempo: __Hoje__; para pedidos realizados no mesmo dia, __Ontem__; para os pedidos realizados no dia anterior, __Últimos 7 dias__; para os pedidos realizados nos últimos 7 dias, e assim por diante nos últimos 30 dias e no último ano. Existe também a possíbilidade de filtrar por um período personalizado conforme necessário.

No primeiro quadro é mostrado um gráfico de pizza contendo a forma de pagamento distribuida pelo valor de venda, que seria equivalente a saber qual forma de pagamento obteve o maior valor de transação, exemplo: Por boleto bancário; valor de R$200,00 e para cartão de crédito o valor foi de R$1.200,00, de um total de R$1.400,00 de venda.  
![screenshot_report_pagamento](assets/img/prints/reports/valorvenda.png)

--- 

No segundo quadro é mostrado um gráfico semelhante, porém distribuido pela quantidade de pedidos pagos por aquela forma de pagamento, por exemplo: dos 10 pedidos que foram realizados no período, 5 foram boletos e 5 foram por cartão de crédito.
![screenshot_report_pagamento](assets/img/prints/reports/qtdpedido.png)

Conforme imagem abaixo, ainda é possível ter uma visão de números dos mesmos dados apresentados nos gráficos acima, além da possibilidade de imprimir o relatório.

![screenshot_report_pagamento](assets/img/prints/reports/formapagamento.png "Relatório de Formas de Pagamento")

Obs.: A impressão deste relatório mostra apenas os valores apresentados na lateral, onde contem a listagem dos gráficos apresentados. Não são impressos os gráficos de pizza.

---

## Novos clientes

No relatório de novos clientes é possível verificar a quantidade de novos cadastros realizados em um período de tempo. Semelhante ao relatório de _Formas de Pagamento_ também é possível filtrar por períodos de tempo.

No gráfico é apresentado a quantidade de clientes novos por data. Veja mais no print abaixo:

![screenshot_report_pagamento](assets/img/prints/reports/clientesnovos.png "Relatório de Novos Clientes")

---

## Produtos mais vendido

No relatório de produtos mais vendidos é possível visualizar os produtos mais vendidos no site utilizando filtros de períodos de tempo.
É possível também agrupar por variação ou por produtos.

Veja mais no print abaixo:

![screenshot_report_pagamento](assets/img/prints/reports/maisvendido.png "Relatório de Produtos mais vendidos")

---

## Volume de Vendas 
Este relatório irá mostrar o volume de vendas para um período de tempo específico. Lembrando que o relatório irá agrupar pela data de confirmação de pagamento do pedido.

Veja mais abaixo:

![screenshot_report_pagamento](assets/img/prints/reports/volumevenda.png "Relatório de Volume de Vendas")
